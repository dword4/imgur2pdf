#!/usr/bin/python3
from imgurarchive import ImgurArchive
from configparser import ConfigParser
import argparse

config = ConfigParser()
config.read('settings.cfg')

a = ImgurArchive()

parser = argparse.ArgumentParser(description='imgur2pdf - convert an imgur gallery to pdf for archival purposes')
parser.add_argument('album', metavar='album',  help='id of imgur album')
parser.add_argument('-d', metavar='destination', help='location to save to')

parser.add_argument('-t', help='type of document to store as (word, pdf)')
args = parser.parse_args()

letsGo = True # this is our go/no-go value

# file-type check first
if args.t is None:
    file_type = config.get('global','default_type')
else:
    # we need to check and make sure its valid input
    if args.t.lower() in a._filetypes:
        file_type = args.t
    else:
        print("invalid type, options are: word, pdf")
        letsGo = False


# destination check 
if args.d is None:
    # no destination given, fall back to config default
    destination = config.get('global','default_location')
else:
    if a.CheckDestination(args.d) is True:
        # location passes muster
        destination = args.d
    else:
        print("invalid destination or bad permissions")
        letsGo = False

if letsGo == True:
    # time to archive!
    if file_type == 'pdf':
        a.ArchivePDF(args.album, destination)
    elif file_type == 'word':
        a.ArchiveWord(args.album, destination)
