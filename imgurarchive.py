#!/usr/bin/python

from PIL import Image
from creds import * 
import PIL
from reportlab.pdfgen import canvas
from reportlab.platypus import SimpleDocTemplate, Paragraph, Image, Spacer, PageBreak
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.pagesizes import letter
from reportlab.lib.units import inch
from reportlab.lib.enums import TA_JUSTIFY
from docx import Document
from docx.shared import Inches
import requests
import progressbar as pb
import shutil
import os
import argparse
import sys
import json

if sys.version_info[0]== 2:
    pass 
elif sys.version_info[0] == 3:
    from builtins import input

class ImgurArchive:

    _filetypes = ['pdf','word']

    def ArchivePDF(self, url, destination):
        # links from the website must be converted for API use
        url_split = url.split('/')
        album_id = url_split[-1]
        new_url = "https://api.imgur.com/3/album/"+album_id
        url = new_url 

        headers = {'Authorization':'Client-ID '+client_id}
        r = requests.get(url, headers=headers)
        album = json.loads(r.text)

        album_id = album['data']['id']
        if album['data']['title'] is None:
            album_title = 'no title found'
        else:
            album_title = album['data']['title']
        album_datetime = album['data']['datetime']

        # filter list, may need expansion/revision later

        filter_list = {
            ' ':'_',
            '/':'-',
            '|':'-'
        }
        for k, m in enumerate(filter_list):
            album_title = album_title.replace(m, filter_list[m])
        album_file = album_title+".pdf"
        path = destination + '/' + album_file
        '''
        if os.path.isfile(path) == True:
            # we found something!
            print("found file %s, try with another destination" % path)
            quit()
        else:
            # nothing found, lets make stuff
            pass
        '''

        doc = SimpleDocTemplate(path,pagesize=letter,
                                rightMargin=25,leftMargin=25,
                                topMargin=25,bottomMargin=25)
        ParagraphStyle(name = 'Normal',
                       fontName = "Verdana",
                       fontSize = 11,
                       leading = 15,
                       alignment = TA_JUSTIFY,
                       allowOrphans = 0,
                       spaceBefore = 20,
                       spaceAfter = 20,
                       wordWrap = 1)
        Story=[]
        styles=getSampleStyleSheet()

        items = album['data']['images']

        p = 0 
        for item in items:
            p += 1
        bar = pb.ProgressBar(maxval=p).start()
        p = 1

        for item in items:

            image_height = item['height']
            image_width = item['width']
            image_link = item['link']
            image_id = item['id']
            if item['title'] is not None:
                image_title = item['title']
            else:
                image_title = ''
            image_desc = item['description']

            response = requests.get(image_link, stream=True)
            name = str(image_id)+".jpg"
            with open(name, 'wb') as out_file:
                shutil.copyfileobj(response.raw, out_file)
            del response
            sc = PIL.Image.open(name)
            width, height = sc.size

            # time to look at the size and apply a resize ratio
            if height <= 600 and width <= 800:
                resize_ratio = 0.50
            else :
                resize_ratio = 0.15

            scaled_width = width * resize_ratio
            scaled_height = height * resize_ratio 

            im = Image(name, scaled_width, scaled_height)
            title = str(image_title)
            if image_title:
                Story.append(Paragraph(image_title, styles["Normal"]))
            Story.append(im)
            if image_desc:
                Story.append(Paragraph(image_desc, styles["Normal"]))
            Story.append(PageBreak())
            
            bar.update(p)
            p += 1
           
        doc.build(Story)
        print("\nfile created -> "+str(path))
        if os.system == 'posix':
            os.system("rm *.jpg")
            os.system("rm *.png")
        elif os.system == 'nt':
            os.system("del *.jpg")
            os.system("del *.png")

        # maybe a nice clean return here at some point? 

    def ArchiveWord(self, url, destination):

        document = Document()
        # links from the website must be converted for API use
        url_split = url.split('/')
        album_id = url_split[-1]
        new_url = "https://api.imgur.com/3/album/"+album_id
        url = new_url 

        headers = {'Authorization':'Client-ID '+client_id}
        r = requests.get(url, headers=headers)
        album = json.loads(r.text)

        album_id = album['data']['id']
        if album['data']['title'] is None:
            album_title = 'no title found'
        else:
            album_title = album['data']['title']
        album_datetime = album['data']['datetime']

        # filter list, may need expansion/revision later

        filter_list = {
            ' ':'_',
            '/':'-',
            '|':'-'
        }
        for k, m in enumerate(filter_list):
            album_title = album_title.replace(m, filter_list[m])
        album_file = album_title+".docx"
        path = destination + '/' + album_file

        items = album['data']['images']

        p = 0 
        for item in items:
            p += 1
        bar = pb.ProgressBar(maxval=p).start()
        p = 1

        for item in items:

            image_height = item['height']
            image_width = item['width']
            image_link = item['link']
            image_id = item['id']
            if item['title'] is not None:
                image_title = item['title']
            else:
                image_title = ''
            image_desc = item['description']

            response = requests.get(image_link, stream=True)
            name = str(image_id)+".jpg"
            with open(name, 'wb') as out_file:
                shutil.copyfileobj(response.raw, out_file)
            del response
            sc = PIL.Image.open(name)
            width, height = sc.size

            # time to look at the size and apply a resize ratio
            if height <= 600 and width <= 800:
                resize_ratio = 0.50
            else :
                resize_ratio = 0.15

            scaled_width = width * resize_ratio
            scaled_height = height * resize_ratio 

            im = Image(name, scaled_width, scaled_height)
            new_name = str(image_id)+".png"
            # try to save a png
            im2 = PIL.Image.open(name)
            im2.save(new_name)
            title = str(image_title)
            if image_title:
                document.add_heading(image_title)
            #  might want to make a default size in the config later
            document.add_picture(new_name, width=Inches(5.5))
            if image_desc:
                document.add_paragraph(image_desc)
            document.add_page_break()
            
            bar.update(p)
            p += 1
           
        document.save(path)
        print("\nfile created -> "+str(path))
        if os.system == 'posix':
            os.system("rm *.jpg")
            os.system("rm *.png")
        elif os.system == 'nt':
            os.system("del *.jpg")
            os.system("del *.png")

    def CheckDestination(self, destination):
        if os.path.isdir(destination) is True and os.path.exists(destination) is True:
            return True
        else:
            return False
        

